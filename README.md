# Server requirements:

```
#!shell


sudo apt-get update
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password >>>REPLACE_THIS_with_a_Pa55WORD<<<'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password >>>REPLACE_THIS_with_a_Pa55WORD<<<'
sudo apt-get --assume-yes install git apache2 php5 mysql-server php5-mcrypt php5-mysql php5-json php5-curl php5-gd nodejs nodejs-legacy npm
```

# Server configuration

edit /etc/apache2/apache2.conf

edit <Directory /var/www/> to look like this:

```
#!

<Directory /var/www/>
        Options Indexes FollowSymLinks
        AllowOverride all
        Require all granted
</Directory>

```



sudo a2enmod rewrite

sudo service apache2 restart


# Installation wizard for October

The wizard installation is a recommended way to install October. It is simpler than the command-line installation and doesn't require any special skills.

1. cd /var/www/html
1. rm index.html
1. curl -sS https://github.com/octobercms/install/archive/master.zip >> master.zip
1. unzip master.zip .
1. sudo chmod 775 -R *
1. sudo chown www-data:www-data -R *
1. autoservisas24.pro/install.php
1. Install with Squad theme
1. Install the WysiWyg editors plugin
1. Install october.Drivers plugin (required for email)

## Minimum System Requirements

October CMS has a few system requirements:

* PHP 5.5.9 or higher
* PDO PHP Extension
* cURL PHP Extension
* MCrypt PHP Extension
* ZipArchive PHP Library
* GD PHP Library

As of PHP 5.5, some OS distributions may require you to manually install the PHP JSON extension.
When using Ubuntu, this can be done via ``apt-get install php5-json``.